# VIM Configuration

## Installation

### Automatic

Download and run the installer shell script. This creates a backup of your old
vim configuration and downloads this repository and vundle.

    curl https://bitbucket.org/c14n/vimconfig/raw/master/install.sh | bash

### Manual

Clone this repository and vundle:

    git clone https://bitbucket.org/c14n/vimconfig.git "$HOME/.vim"
    git clone https://github.com/gmarik/Vundle.vim.git "$HOME/.vim/bundle/Vundle.vim"

Then start vim and run :BundleInstall.
